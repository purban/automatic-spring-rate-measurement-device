Version 4
SHEET 1 880 680
WIRE 208 -96 -80 -96
WIRE 256 -96 208 -96
WIRE 480 -96 256 -96
WIRE 16 -32 -304 -32
WIRE 288 -16 144 -16
WIRE -304 16 -304 -32
WIRE 208 32 208 -96
WIRE 592 32 512 32
WIRE 144 48 144 -16
WIRE 176 48 144 48
WIRE 288 64 288 -16
WIRE 288 64 240 64
WIRE 16 80 16 -32
WIRE 176 80 16 80
WIRE 480 112 480 -96
WIRE 368 128 368 64
WIRE 432 128 432 32
WIRE 432 128 368 128
WIRE 448 128 432 128
WIRE -304 144 -304 96
WIRE -304 144 -448 144
WIRE 592 144 592 32
WIRE 592 144 512 144
WIRE -80 160 -80 -96
WIRE 208 160 208 96
WIRE 208 160 112 160
WIRE 432 160 368 160
WIRE 448 160 432 160
WIRE 256 192 256 -96
WIRE 256 192 208 192
WIRE 288 240 128 240
WIRE 368 240 368 160
WIRE 48 256 -288 256
WIRE 208 272 208 192
WIRE 128 288 128 240
WIRE 176 288 128 288
WIRE 288 304 288 240
WIRE 288 304 240 304
WIRE 48 320 48 256
WIRE 176 320 48 320
WIRE -448 400 -448 144
WIRE -288 400 -288 336
WIRE -288 400 -448 400
WIRE -80 400 -80 240
WIRE -80 400 -288 400
WIRE 112 400 112 160
WIRE 112 400 -80 400
WIRE 208 400 208 336
WIRE 208 400 112 400
WIRE 336 400 208 400
WIRE 432 400 432 240
WIRE 432 400 336 400
WIRE 480 400 480 176
WIRE 480 400 432 400
WIRE 336 432 336 400
WIRE 0 0 0 0
FLAG 336 432 0
SYMBOL Opamps\\opamp2 208 0 R0
SYMATTR InstName U1
SYMBOL res 384 48 R90
WINDOW 0 0 56 VBottom 0
WINDOW 3 32 56 VTop 0
SYMATTR InstName R1
SYMATTR Value 100
SYMBOL res 384 224 R90
WINDOW 0 0 56 VBottom 0
WINDOW 3 32 56 VTop 0
SYMATTR InstName R2
SYMATTR Value 100
SYMBOL res 528 16 R90
WINDOW 0 0 56 VBottom 0
WINDOW 3 32 56 VTop 0
SYMATTR InstName R3
SYMATTR Value 10000
SYMBOL res 448 256 R180
WINDOW 0 34 65 Left 0
WINDOW 3 36 40 Left 0
SYMATTR InstName R4
SYMATTR Value 10000
SYMBOL Opamps\\opamp2 208 240 R0
WINDOW 3 15 97 Left 0
SYMATTR InstName U2
SYMBOL Opamps\\opamp2 480 80 R0
SYMATTR InstName U3
SYMBOL Misc\\battery -80 144 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 10
SYMBOL Misc\\battery -304 0 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V2
SYMATTR Value 4,65
SYMBOL Misc\\battery -288 240 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V3
SYMATTR Value 4,71
TEXT -450 456 Left 0 !.tran 100
