﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;
using TextDateien;
using System.IO;
using System.Drawing.Drawing2D;
using CarlosAg.ExcelXmlWriter;

namespace Automatischer_Federkraftmesser 
{
    public partial class Form1 : Form
    {
        #region Parameter Initialisierung
        private bool _boSoftwareSettingsChanged = false;
        private bool _boSettingsTabLeave = false;
        private bool _boIntervalStatus = false;
        private bool _boTaraSet = false;
        private bool _boDeviceSettingsChanged = false;
        //1 -> isClose | 0 -> isOpen
        private int _iPortStatus = 1;
        private int _iGetHWinfo = 0;
        private int _iSendSettings = 0;
        private int _iReloadCount;
        private int _iGetMeasuredValues = 0;
        private int _iDeviceConfigSTL = 0;
        private int _iDeviceConfigSR5 = 0;
        private int _iDeviceConfigSR6 = 0;
        private int _iDeviceConfigSLO = 0;
        private int _iDeviceConfigSRG = 0;
        private int _iDeviceConfigSLI = 0;
        private int _iDeviceConfigSLA = 0;
        private int _iTransmissionInterval;
        private int _iResponseError = 100;
        private int _iSetTara = 0;
        private double _fGravAcceleration;
        private double _fMeasuredDistance;
        private double _fMeasuredForce;
        private double _fMeasuredSpringConst;
        private double _fMeasuredTaraForce;
        private double _fMeasuredTaraDistance;
        private double _fMeasuredDeltaForce;
        private double _fMeasuredDeltaDistance;
        private string _strHardwareInfo = "";
        private string _strFirmwareInfo = "";
        const string _strConfigFileName = "config.cfg";
        const string _VERBINDEN_ = "Verbinden";
        const string _TRENNEN_ = "Trennen";
        const string _GETRENNT_ = "getrennt";
        TextDatei _txtConfigFile = new TextDatei();
        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Portliste initialisieren
            comboBox1.Items.AddRange(SerialPort.GetPortNames());
            comboBox1.SelectedIndex = 0;
            dataFormat.SelectedIndex = 0;

            //Config initialisieren
            LoadConfig();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serialPort1.IsOpen)
                serialPort1.Close();
        }

        #region Textfelder Geräteeinstellungen 

        private void transInterval_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = CheckIfNumber(sender, e);   
        }

        private void gravAcceleration_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = CheckIfNumber(sender, e, true);
        }

        private void deviceSettings_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = CheckIfNumber(sender, e);
        }

        private void softwareSettingFieldChanged_KeyUp(object sender, KeyEventArgs e)
        {
            _boSoftwareSettingsChanged = true;
            SettingsChanged();
        }

        private void deviceSettingFieldChanged_KeyUp(object sender, KeyEventArgs e)
        {
            _boDeviceSettingsChanged = true;
            SettingsChanged();
        }
        #endregion

        private void LoadConfig()
        {

            string configContent;

            //Neues File schreiben falls nich schon vorhanden
            if (!File.Exists(_strConfigFileName))
            {
                configContent =
                    "#Geraeteeinstellungen\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n0\r\n#Softwareeinstellungen\r\n500\r\n9.81\r\n";
                _txtConfigFile.WriteFile(_strConfigFileName, configContent);
            }

            //Load Config
            configContent = _txtConfigFile.ReadFile(_strConfigFileName);
            string[] configContentSplit = configContent.Split(new string[] { "\r\n" }, StringSplitOptions.None); //Split String with String

            _iDeviceConfigSTL = System.Convert.ToInt16(configContentSplit[1]);
            _iDeviceConfigSR5 = System.Convert.ToInt16(configContentSplit[2]);
            _iDeviceConfigSR6 = System.Convert.ToInt16(configContentSplit[3]);
            _iDeviceConfigSLO = System.Convert.ToInt16(configContentSplit[4]);
            _iDeviceConfigSRG = System.Convert.ToInt16(configContentSplit[5]);
            _iDeviceConfigSLI = System.Convert.ToInt16(configContentSplit[6]);
            _iDeviceConfigSLA = System.Convert.ToInt16(configContentSplit[7]);
            _iTransmissionInterval = System.Convert.ToInt16(configContentSplit[9]);
            timer2.Interval = _iTransmissionInterval;
            if (configContentSplit[10].IndexOf(",") != -1)
                configContentSplit[10] = configContentSplit[10].Replace(',', '.');
            _fGravAcceleration = System.Convert.ToDouble(configContentSplit[10], System.Globalization.CultureInfo.InvariantCulture);
            
            stl.Text = _iDeviceConfigSTL.ToString();
            sr5.Text = _iDeviceConfigSR5.ToString();
            sr6.Text = _iDeviceConfigSR6.ToString();
            slo.Text = _iDeviceConfigSLO.ToString();
            srg.Text = _iDeviceConfigSRG.ToString();
            sli.Text = _iDeviceConfigSLI.ToString();
            sla.Text = _iDeviceConfigSLA.ToString();
            transInterval.Text = _iTransmissionInterval.ToString();
            gravAcceleration.Text = _fGravAcceleration.ToString();
        }

        private void SaveConfig()
        {
            _iDeviceConfigSTL = System.Convert.ToInt16(stl.Text);
            _iDeviceConfigSR5 = System.Convert.ToInt16(sr5.Text);
            _iDeviceConfigSR6 = System.Convert.ToInt16(sr6.Text);
            _iDeviceConfigSLO = System.Convert.ToInt16(slo.Text);
            _iDeviceConfigSRG = System.Convert.ToInt16(srg.Text);
            _iDeviceConfigSLI = System.Convert.ToInt16(sli.Text);
            _iDeviceConfigSLA = System.Convert.ToInt16(sla.Text);
            _iTransmissionInterval = System.Convert.ToInt16(transInterval.Text);
            timer2.Interval = _iTransmissionInterval;
            if (gravAcceleration.Text.IndexOf(",") != -1)
                gravAcceleration.Text = gravAcceleration.Text.Replace(',', '.');
            _fGravAcceleration = System.Convert.ToDouble(gravAcceleration.Text, System.Globalization.CultureInfo.InvariantCulture);

            string configContent;
            configContent =
                string.Format("#Geraeteeinstellungen\r\n{0}\r\n{1}\r\n{2}\r\n{3}\r\n{4}\r\n{5}\r\n{6}\r\n#Softwareeinstellungen\r\n{7}\r\n{8:0.00}\r\n",
                _iDeviceConfigSTL.ToString(),
                _iDeviceConfigSR5.ToString(),
                _iDeviceConfigSR6.ToString(),
                _iDeviceConfigSLO.ToString(),
                _iDeviceConfigSRG.ToString(),
                _iDeviceConfigSLI.ToString(),
                _iDeviceConfigSLA.ToString(),
                _iTransmissionInterval.ToString(),
                _fGravAcceleration.ToString()
                );
            _txtConfigFile.WriteFile(_strConfigFileName, configContent);
        }

        private void SendConfig()
        {
            if (_iPortStatus == 0)
            {
                StopAllTransmissions();
                saveDeviceConfig.Enabled = false;
                _iSendSettings = 1;
                saveConfigInfoPic.Visible = true;
                saveConfigInfoPic.Image = Properties.Resources.loading;
                _iReloadCount = 20;
            }
        }

        private void SwitchMeasurementTransmission()
        {
            if (timer2.Enabled)
            {
                timer2.Enabled = false;
                _iGetMeasuredValues = 0;
                uebertragungStart.Text = "Übertragung starten";
                measuredDistance.Text = "-";
                measuredForce.Text = "-";
                federkonstante.Text = "-";
                //measurmentInfo.Text = "";
            }
            else
            {
                timer2.Enabled = true;
                uebertragungStart.Text = "Übertragung beenden";
            }
        }

        private void SettingsChanged(bool mode = true)
        {
            if (mode)
            {
                saveDeviceConfig.ForeColor = Color.Red;
            }
            else
            {
                _boSoftwareSettingsChanged = false;
                _boDeviceSettingsChanged = false;
                saveDeviceConfig.ForeColor = Color.Black;
            }
        }

        private void StopAllTransmissions()
        {
            _iGetMeasuredValues = 0;
            _boIntervalStatus = timer2.Enabled;
            timer2.Enabled = false;

            _iGetHWinfo = 0;
            _iSetTara = 0;
            rs232._strResponse = "";
            rs232._boAbortThread = true;
        }

        private void UpdateDeviceInfo()
        {
            if (_iPortStatus == 0)
            {
                verbindenButton.Enabled = false;
                pictureBox1.Image = Properties.Resources.loading;
                _iGetHWinfo = 1;
            }
            else
            {
                _iGetHWinfo = 4;
                _iReloadCount = 5;
                pictureBox1.Image = Properties.Resources._false;
                deviceStatus.Text = _GETRENNT_;
            }
            rs232._strResponse = "";
        }

        private void ChangeConnectionStatus(bool status)
        {
            if (status)
            {
                verbindenButton.ForeColor = Color.Black;
                verbindenButton.Text = _TRENNEN_;
                _iPortStatus = 0;
                pictureBox1.Visible = true;
                saveDeviceConfig.Text = "Speichern und Übertragen";
                stl.Enabled = true;
                sr5.Enabled = true;
                sr6.Enabled = true;
                slo.Enabled = true;
                srg.Enabled = true;
                sli.Enabled = true;
                sla.Enabled = true;
                uebertragungStart.Enabled = true;
                nullpunktSetzen.Enabled = true;
                //timer2.Enabled = true;
                UpdateDeviceInfo();
            }
            else
            {
                verbindenButton.ForeColor = Color.Red;
                verbindenButton.Text = _VERBINDEN_;
                hardwareInfo.Text = "-";
                firmwareInfo.Text = "-";
                verbindungstyp.Text = "-";
                saveDeviceConfig.Text = "Speichern";
                stl.Enabled = false;
                sr5.Enabled = false;
                sr6.Enabled = false;
                slo.Enabled = false;
                srg.Enabled = false;
                sli.Enabled = false;
                sla.Enabled = false;
                uebertragungStart.Enabled = false;
                nullpunktSetzen.Enabled = false;
                timer2.Enabled = false;
                if (_iGetHWinfo != 4)
                {
                    deviceStatus.Text = _GETRENNT_;
                    pictureBox1.Visible = false;
                    _iGetHWinfo = 0;

                }                    
                _iGetMeasuredValues = 0;
                _iSendSettings = 0;
                _iPortStatus = 1;
            }
        }

        private void ChangePortStatus(int status)
        {
            switch (rs232.SerialPortChangeStatus(serialPort1, status))
            {
                case 0:
                    if (status == 1)
                    {
                        ChangeConnectionStatus(true);
                    }
                    else
                    {
                        ChangeConnectionStatus(false);
                    }
                    break;
                case 1:
                    MessageBox.Show(
                    "Auf die gewählte Schnittstelle konnte nicht zugregriffen werden. \nEventuell wird diese bereits von einem anderen Programm genutzt.",
                    "Fehler beim Zugriff auf Schnittstelle!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1);
                    
                    break;
                case 2:
                    if (status == 1)
                    {
                        ChangeConnectionStatus(true);
                    }
                    else
                    {
                        ChangeConnectionStatus(false);
                    }
                    break;
            }
        }

        private bool CheckIfNumber(object sender, KeyEventArgs e, bool allowFloat = false)
        {
            bool SupressKeyPress = false;
            //Allow navigation keyboard arrows
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                case Keys.PageUp:
                case Keys.PageDown:
                    return false;
                default:
                    break;
            }

            if(!allowFloat && (e.KeyValue == 188 || e.KeyValue == 190))
                return true;
            //Block non-number characters
            char currentKey = (char)e.KeyCode;
            bool modifier = e.Control || e.Alt || e.Shift;
            bool nonNumber = char.IsLetter(currentKey) ||
                             char.IsSymbol(currentKey) ||
                             char.IsWhiteSpace(currentKey) ||
                             char.IsPunctuation(currentKey);

            if (!modifier && nonNumber)
                SupressKeyPress = true;

            //Handle pasted Text
            if (e.Control && e.KeyCode == Keys.V)
            {
                //Preview paste data (removing non-number characters)
                string pasteText = Clipboard.GetText();
                string strippedText = "";
                for (int i = 0; i < pasteText.Length; i++)
                {
                    if (char.IsDigit(pasteText[i]))
                        strippedText += pasteText[i].ToString();
                }

                if (strippedText != pasteText)
                {
                    //There were non-numbers in the pasted text
                    SupressKeyPress = true;

                    //OPTIONAL: Manually insert text stripped of non-numbers
                    TextBox me = (TextBox)sender;
                    int start = me.SelectionStart;
                    string newTxt = me.Text;
                    newTxt = newTxt.Remove(me.SelectionStart, me.SelectionLength); //remove highlighted text
                    newTxt = newTxt.Insert(me.SelectionStart, strippedText); //paste
                    me.Text = newTxt;
                    me.SelectionStart = start + strippedText.Length;
                }
                else
                    SupressKeyPress = false;
            }
            //NumPad
                if(e.KeyCode.ToString().IndexOf("NumPad") != -1)
                    SupressKeyPress = false;

            return SupressKeyPress;
        }

        private static Image ResizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        private void AddDataRow(double force, double distance, double springConst, bool tara)
        {
            Image image;
            if (tara)
                image = Properties.Resources._checked;
            else
                image = Properties.Resources._false;
            Size size = new Size(16, 16);
            image = ResizeImage(image, size);
            DateTime date = DateTime.Now;

            int n = dataGridView1.Rows.Add();
            dataGridView1.Rows[n].Cells[0].Value = dataGridView1.Rows.Count;
            dataGridView1.Rows[n].Cells[1].Value = date.ToString("dd.MM.yyyy - HH:mm:ss");
            dataGridView1.Rows[n].Cells[2].Value = force.ToString();
            dataGridView1.Rows[n].Cells[3].Value = distance.ToString();
            dataGridView1.Rows[n].Cells[4].Value = springConst.ToString();
            dataGridView1.Rows[n].Cells[5].Value = image;
            if(tara)
                dataGridView1.Rows[n].Cells[6].Value = "true";
            else
                dataGridView1.Rows[n].Cells[6].Value = "false";

            this.dataGridView1.CurrentCell = this.dataGridView1[0, this.dataGridView1.Rows.Count - 1];
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            UpdateDeviceInfo();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(_iPortStatus==1)
                serialPort1.PortName = comboBox1.SelectedItem.ToString();
            ChangePortStatus(_iPortStatus);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            #region Geräteinformationen Abrufen
            if (_iGetHWinfo > 0)
            {
                if (_iGetHWinfo == 4)
                {
                    verbindenButton.Enabled = true;
                    if (_iReloadCount-- <= 0)
                    {
                        if (_iPortStatus == 1)
                        {
                            deviceStatus.Text = _GETRENNT_;
                            pictureBox1.Visible = false;
                        }
                        _iGetHWinfo = 0;
                    }
                    
                }
                if(!rs232._boBusy && rs232._strResponse == "")
                {
                    switch (_iGetHWinfo)
                    {
                        case 1:
                            rs232.SendCommand(serialPort1, "thi;");
                            break;
                        case 2:
                            rs232.SendCommand(serialPort1, "tfi;");
                            break;
                        case 3:
                            hardwareInfo.Text = _strHardwareInfo;
                            firmwareInfo.Text = _strFirmwareInfo;
                            pictureBox1.Image = Properties.Resources._checked;
                            _iReloadCount = 10;
                            _iGetHWinfo = 4;
                            deviceStatus.Text = "verbunden";
                            if (comboBox1.SelectedItem.ToString().IndexOf("COM") != -1)
                                verbindungstyp.Text = "COM/Seriell";
                            _iResponseError = 0;
                            break;
                    }
                }
                if (_iGetHWinfo > 0 && _iGetHWinfo != 4 && !rs232._boBusy && rs232._strResponse != "")
                {
                    string response = rs232._strResponse;

                    if (response != "err 199")
                    {
                        string strPattern;
                        strPattern = @"rhi ([0-9]*)";
                        Regex regexRHI = new Regex(strPattern);
                        Match matchRHI = regexRHI.Match(response);
                        if (matchRHI.Success)
                           _strHardwareInfo = matchRHI.Groups[1].Value.Insert(2, ".").Insert(5, ".20");

                        strPattern = @"rfi ([0-9]*)";
                        Regex regexRFI = new Regex(strPattern);
                        Match matchRFI = regexRFI.Match(response);
                        if (matchRFI.Success)
                            _strFirmwareInfo = matchRFI.Groups[1].Value.Insert(2, ".").Insert(5, ".20");

                        _iGetHWinfo++;
                    }
                    else
                    {
                        pictureBox1.Image = Properties.Resources._false;
                        _iReloadCount = 20;
                        _iGetHWinfo = 4;
                        deviceStatus.Text = "nicht gefunden";
                        ChangePortStatus(0);
                    }
                    rs232._strResponse = "";
                }
            }
            #endregion

            #region Messwerte abrufen
            if (_iGetMeasuredValues > 0 && timer2.Enabled)
            {
                if (!rs232._boBusy && rs232._strResponse == "")
                {
                    switch (_iGetMeasuredValues)
                    {
                        case 1:
                            rs232.SendCommand(serialPort1, "tmd;");
                            break;
                        case 2:
                            rs232.SendCommand(serialPort1, "tmf;");
                            break;
                        case 3:
                            bool showMeasuredValues = false;
                            switch (_iResponseError)
                            {
                                case 100:
                                    measurmentInfo.Text = "Messwerte erfolgreich übertragen.";
                                    showMeasuredValues = true;
                                    break;
                                case 102:
                                    measurmentInfo.Text = "Die Kraftmessdose ist unterhalb des zulässigen Minimums belastet!";
                                    break;
                                case 103:
                                    measurmentInfo.Text = "Auf dem Gerät wurde noch kein Nullpunkt gesetzt!";
                                    showMeasuredValues = true;
                                    break;
                                case 199:
                                    measurmentInfo.Text = "Messwerte konnten nicht übertragen werden! Verbindung Überprüfen oder Gerät neu starten.";
                                    SwitchMeasurementTransmission();
                                    ChangePortStatus(0);
                                    _iResponseError = 0;
                                    break;
                            }
                            if (!_boTaraSet && showMeasuredValues)
                            {
                                measurmentInfo.Text = "Achtung noch kein Nullpunkt gesetzt!";
                            }

                            if (showMeasuredValues)
                            {
                                _fMeasuredDeltaDistance = Math.Round(_fMeasuredDistance - _fMeasuredTaraDistance,2);
                                _fMeasuredDeltaForce = Math.Round(_fMeasuredForce - _fMeasuredTaraForce,2);
                                measuredDistance.Text = _fMeasuredDeltaDistance.ToString() + " mm";
                                measuredForce.Text = _fMeasuredDeltaForce.ToString() + " kg";
                                if (_fMeasuredDeltaDistance > 0)
                                {
                                    _fMeasuredSpringConst = Math.Round((_fMeasuredDeltaForce * _fGravAcceleration) / (_fMeasuredDeltaDistance), 1);
                                }
                                else
                                {
                                    _fMeasuredSpringConst = 0.0;
                                }
                                federkonstante.Text = _fMeasuredSpringConst.ToString() + " N/mm";
                                AddDataRow(_fMeasuredDeltaForce, _fMeasuredDeltaDistance, _fMeasuredSpringConst, _boTaraSet);
                            }
                            else
                            {
                                measuredDistance.Text = "-";
                                measuredForce.Text = "-";
                                federkonstante.Text = "-";
                            }
                            _iGetMeasuredValues = 0;
                            break;
                    }
                }
                if (_iGetMeasuredValues > 0 && !rs232._boBusy && rs232._strResponse != "")
                {
                    string response = rs232._strResponse;
                        //Weg auslesen
                        string strPattern;
                        strPattern = @"rmd ([-0-9\.]*)";
                        Regex regexRMD = new Regex(strPattern);
                        Match matchRMD = regexRMD.Match(response);
                        if (matchRMD.Success)
                            _fMeasuredDistance = System.Convert.ToDouble(matchRMD.Groups[1].Value, System.Globalization.CultureInfo.InvariantCulture);

                        //Kraft auslesen
                        strPattern = @"rmf ([-0-9\.]*)";
                        Regex regexRMF = new Regex(strPattern);
                        Match matchRMF = regexRMF.Match(response);
                        if (matchRMF.Success)
                            _fMeasuredForce = System.Convert.ToDouble(matchRMF.Groups[1].Value, System.Globalization.CultureInfo.InvariantCulture);

                        //Fehler auslesen
                        strPattern = @"err ([0-9]*)";
                        Regex regexERR = new Regex(strPattern);
                        Match matchERR = regexERR.Match(response);
                        if (matchERR.Success)
                            _iResponseError = System.Convert.ToInt16(matchERR.Groups[1].Value);

                        _iGetMeasuredValues++;
                    rs232._strResponse = "";
                }
            }
            #endregion

            #region Einstellungen übertragen
            if (_iSendSettings > 0)
            {
                if (_iSendSettings == 9)
                {
                    if (_iReloadCount-- <= 0)
                    {
                        saveConfigInfoPic.Visible = false;
                        _iSendSettings = 0;
                    }

                }
                if (!rs232._boBusy && rs232._strResponse == "")
                {
                    switch (_iSendSettings)
                    {
                        case 1:
                            rs232.SendCommand(serialPort1, "stl " + _iDeviceConfigSTL.ToString() + ";");
                            break;
                        case 2:
                            rs232.SendCommand(serialPort1, "sr5 " + _iDeviceConfigSR5.ToString() + ";");
                            break;
                        case 3:
                            rs232.SendCommand(serialPort1, "sr6 " + _iDeviceConfigSR6.ToString() + ";");
                            break;
                        case 4:
                            rs232.SendCommand(serialPort1, "slo " + _iDeviceConfigSLO.ToString() + ";");
                            break;
                        case 5:
                            rs232.SendCommand(serialPort1, "srg " + _iDeviceConfigSRG.ToString() + ";");
                            break;
                        case 6:
                            rs232.SendCommand(serialPort1, "sli " + _iDeviceConfigSLI.ToString() + ";");
                            break;
                        case 7:
                            rs232.SendCommand(serialPort1, "sla " + _iDeviceConfigSLA.ToString() + ";");
                            break;
                        case 8:
                            saveConfigInfoPic.Image = Properties.Resources._checked;
                            timer2.Enabled = _boIntervalStatus; //Messwerte-übertragen ggf. wieder einschalten
                            saveDeviceConfig.Enabled = true;
                            _iReloadCount = 10;
                            _iSendSettings = 9;
                            break;
                    }
                }
                if (_iSendSettings > 0 && !rs232._boBusy && rs232._strResponse != "")
                {
                    string response = rs232._strResponse;

                    //Fehler auslesen
                    string strPattern = @"err ([0-9]*)";
                    Regex regexERR = new Regex(strPattern);
                    Match matchERR = regexERR.Match(response);
                    if (matchERR.Success)
                        _iResponseError = System.Convert.ToInt16(matchERR.Groups[1].Value);

                    switch (_iResponseError)
                    {
                        case 101:
                            saveConfigInfoPic.Image = Properties.Resources._false;
                            ChangePortStatus(0);
                            _iSendSettings = 9;
                            break;
                        case 199:
                            saveConfigInfoPic.Image = Properties.Resources._false;
                            ChangePortStatus(0);
                            _iSendSettings = 9;
                            break;
                        default: _iSendSettings++;
                            break;
                    }


                    rs232._strResponse = "";
                }
            }
            #endregion

            #region Nullpunkt setzen
            if (_iSetTara > 0)
            {
                if (_iSetTara == 5)
                {
                    if (_iReloadCount-- <= 0)
                    {
                        setTaraInfoPic.Visible = false;
                        _iSetTara = 0;
                    }

                }
                if (!rs232._boBusy && rs232._strResponse == "")
                {
                    switch (_iSetTara)
                    {
                        case 1:
                            rs232.SendCommand(serialPort1, "tmd;");
                            break;
                        case 2:
                            rs232.SendCommand(serialPort1, "tmf;");
                            break;
                        case 3:
                            rs232.SendCommand(serialPort1, "svz;");
                            break;
                        case 4:
                            timer2.Enabled = _boIntervalStatus;
                            uebertragungStart.Enabled = true;
                            _iReloadCount = 10;
                            _iSetTara = 5;
                            measurmentInfo.Text = "Nullpunkt erfolgreich gesetzt.";
                            _boTaraSet = true;
                            setTaraInfoPic.Image = Properties.Resources._checked;
                            _iResponseError = 0;
                            break;
                    }
                }
                if (_iSetTara > 0 && !rs232._boBusy && rs232._strResponse != "" && _iGetMeasuredValues==0)
                {
                    string response = rs232._strResponse;
                    //Weg auslesen
                    string strPattern;
                    strPattern = @"rmd ([-0-9\.]*)";
                    Regex regexRMD = new Regex(strPattern);
                    Match matchRMD = regexRMD.Match(response);
                    if (matchRMD.Success)
                        _fMeasuredTaraDistance = System.Convert.ToDouble(matchRMD.Groups[1].Value, System.Globalization.CultureInfo.InvariantCulture);

                    //Kraft auslesen
                    strPattern = @"rmf ([-0-9\.]*)";
                    Regex regexRMF = new Regex(strPattern);
                    Match matchRMF = regexRMF.Match(response);
                    if (matchRMF.Success)
                        _fMeasuredTaraForce = System.Convert.ToDouble(matchRMF.Groups[1].Value, System.Globalization.CultureInfo.InvariantCulture);

                    //Fehler auslesen
                    strPattern = @"err ([0-9]*)";
                    Regex regexERR = new Regex(strPattern);
                    Match matchERR = regexERR.Match(response);
                    if (matchERR.Success)
                        _iResponseError = System.Convert.ToInt16(matchERR.Groups[1].Value);

                    switch (_iResponseError)
                    {
                        case 102:
                            measurmentInfo.Text = "Nullpunkt konnte nicht gesetzt werden, da die Kraftmessdose unterhalb des zulässigen Minimums belastet ist!";
                            setTaraInfoPic.Image = Properties.Resources._false;
                            timer2.Enabled = _boIntervalStatus;
                            uebertragungStart.Enabled = true;
                            _iReloadCount = 10;
                            _iSetTara = 5;
                            break;
                        case 199:
                            measurmentInfo.Text = "Nullpunkt konnte nicht gesetzt werden! Verbindung überprüfen oder Gerät neu starten.";
                            setTaraInfoPic.Image = Properties.Resources._false;
                            _iReloadCount = 10;
                            _iSetTara = 5;
                            SwitchMeasurementTransmission();
                            ChangePortStatus(0);
                            break;
                        default:
                            _iSetTara++;
                            break;
                    }

                    
                    rs232._strResponse = "";
                }
            }
            #endregion
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if(_iGetMeasuredValues==0)
                _iGetMeasuredValues = 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SwitchMeasurementTransmission();
        }

        private void saveDeviceConfig_Click(object sender, EventArgs e)
        {
            SaveConfig();
            if (_boDeviceSettingsChanged && _iPortStatus==0)
                SendConfig();
            if(!_boDeviceSettingsChanged)
            {
                saveConfigInfoPic.Image = Properties.Resources._checked;
                _iReloadCount = 10;
                _iSendSettings = 9;
            }
            SettingsChanged(false);
        }

        private void tabPage2_Leave(object sender, EventArgs e)
        {
            _boSettingsTabLeave = true;
        }

        private void tabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (_boSoftwareSettingsChanged && _boSettingsTabLeave)
            {
                DialogResult answer = MessageBox.Show(
                    "Sie haben Einstellunge verändert, diese jedoch noch nicht gespeichert und an das Gerät übermittelt!\nWollen Sie den Tab trotzdem wechseln?",
                    "Einstellunge wurden nicht gespeichert!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1
                    );
                if (answer == DialogResult.No)
                {
                    e.Cancel = true;
                }

                _boSettingsTabLeave = false;
            }
        }

        private void nullpunktSetzen_Click(object sender, EventArgs e)
        {
            StopAllTransmissions();
            setTaraInfoPic.Image = Properties.Resources.loading;
            setTaraInfoPic.Visible = true;
            uebertragungStart.Enabled = false;
            timer2.Enabled = false; //Messwerte-Übertragen abschalten
            _iSetTara = 1;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            switch (dataFormat.SelectedIndex)
            {
                case 0:
                    saveFileDialog1.Filter = "Excel Datei (*.xls) |*.xls";
                    break;
            }
            DateTime date = DateTime.Now;
            saveFileDialog1.Filter = "Excel Datei (*.xls) |*.xls";
            saveFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            saveFileDialog1.FileName = "Fede-Messwerte_-_Federnummer_" + textBox1.Text + "__(" + date.ToString("dd.MM.yyyy_hh-mm-ss") + ")";
            saveFileDialog1.FilterIndex = 1;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK && saveFileDialog1.FileName.Length > 0)
            {
                Workbook book = new Workbook();
                Worksheet sheet = book.Worksheets.Add("Messwerte");
                WorksheetRow row;

                //Spalten-Überschriften
                row = sheet.Table.Rows.Add();
                row.Cells.Add("Federnummer:");
                row.Cells.Add(textBox1.Text);
                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();
                for (int i = 0; i < dataGridView1.ColumnCount; i++)
                {
                    row.Cells.Add(dataGridView1.Columns[i].HeaderText);
                }
                //Zeileninhalt
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    row = sheet.Table.Rows.Add();
                    for (int iC = 0; iC < dataGridView1.ColumnCount; iC++)
                    {
                        if(iC == 6)
                        {
                            string value;
                            if(dataGridView1.Rows[i].Cells[iC].Value.ToString() == "true")
                                value = "ja";
                            else
                                value = "nein";
                             row.Cells.Add(value);
                        }
                        else if(iC != 5)
                            row.Cells.Add(dataGridView1.Rows[i].Cells[iC].Value.ToString());
                       
                    }
                }
                book.Save(saveFileDialog1.FileName);
            }

            

        }

        private void filtern_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show(
            "Wollen Sie alle Datensätze die Ihre Kriterien erfüllen wirklich unwiederruflich löschen?",
            "Datensätze wirklich löschen?",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question,
            MessageBoxDefaultButton.Button1
            );
            if (answer == DialogResult.Yes)
            {
                bool remove;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    remove = false;
                    if (kraftNull.Checked && dataGridView1.Rows[i].Cells[2].Value.ToString() == "0")
                        remove = true;
                    if (wegNull.Checked && dataGridView1.Rows[i].Cells[3].Value.ToString() == "0")
                        remove = true;
                    if (federkonstanteNull.Checked && dataGridView1.Rows[i].Cells[4].Value.ToString() == "0")
                        remove = true;
                    if (ohneNullpunkt.Checked && dataGridView1.Rows[i].Cells[6].Value.ToString() == "false")
                        remove = true;

                    if (remove)
                        dataGridView1.Rows.RemoveAt(i--);
                }
            }
        }

    }
}
