﻿namespace Automatischer_Federkraftmesser
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.saveConfigInfoPic = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.transInterval = new System.Windows.Forms.TextBox();
            this.gravAcceleration = new System.Windows.Forms.TextBox();
            this.srg = new System.Windows.Forms.TextBox();
            this.sla = new System.Windows.Forms.TextBox();
            this.sli = new System.Windows.Forms.TextBox();
            this.slo = new System.Windows.Forms.TextBox();
            this.sr6 = new System.Windows.Forms.TextBox();
            this.sr5 = new System.Windows.Forms.TextBox();
            this.stl = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.saveDeviceConfig = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ohneNullpunkt = new System.Windows.Forms.CheckBox();
            this.wegNull = new System.Windows.Forms.CheckBox();
            this.kraftNull = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.federkonstanteNull = new System.Windows.Forms.CheckBox();
            this.filtern = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataFormat = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ldf_nr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.force = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.springConst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tara = new System.Windows.Forms.DataGridViewImageColumn();
            this.nullpunktInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.setTaraInfoPic = new System.Windows.Forms.PictureBox();
            this.nullpunktSetzen = new System.Windows.Forms.Button();
            this.measurmentInfo = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.federkonstante = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uebertragungStart = new System.Windows.Forms.Button();
            this.measuredForce = new System.Windows.Forms.Label();
            this.measuredDistance = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.verbindenButton = new System.Windows.Forms.Button();
            this.deviceStatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.firmwareInfo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.verbindungstyp = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.hardwareInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveConfigInfoPic)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setTaraInfoPic)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 200;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(791, 389);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Hilfe";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(302, 183);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Autor und Lizenz";
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(6, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(298, 151);
            this.label30.TabIndex = 0;
            this.label30.Text = resources.GetString("label30.Text");
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.saveConfigInfoPic);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.transInterval);
            this.tabPage2.Controls.Add(this.gravAcceleration);
            this.tabPage2.Controls.Add(this.srg);
            this.tabPage2.Controls.Add(this.sla);
            this.tabPage2.Controls.Add(this.sli);
            this.tabPage2.Controls.Add(this.slo);
            this.tabPage2.Controls.Add(this.sr6);
            this.tabPage2.Controls.Add(this.sr5);
            this.tabPage2.Controls.Add(this.stl);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.saveDeviceConfig);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(791, 389);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Einstellungen";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Leave += new System.EventHandler(this.tabPage2_Leave);
            // 
            // saveConfigInfoPic
            // 
            this.saveConfigInfoPic.Location = new System.Drawing.Point(314, 300);
            this.saveConfigInfoPic.Name = "saveConfigInfoPic";
            this.saveConfigInfoPic.Size = new System.Drawing.Size(23, 23);
            this.saveConfigInfoPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.saveConfigInfoPic.TabIndex = 28;
            this.saveConfigInfoPic.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(664, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "m/s²";
            // 
            // transInterval
            // 
            this.transInterval.Location = new System.Drawing.Point(611, 65);
            this.transInterval.Name = "transInterval";
            this.transInterval.Size = new System.Drawing.Size(47, 20);
            this.transInterval.TabIndex = 8;
            this.transInterval.KeyDown += new System.Windows.Forms.KeyEventHandler(this.transInterval_KeyDown);
            this.transInterval.KeyUp += new System.Windows.Forms.KeyEventHandler(this.softwareSettingFieldChanged_KeyUp);
            // 
            // gravAcceleration
            // 
            this.gravAcceleration.Location = new System.Drawing.Point(611, 97);
            this.gravAcceleration.Name = "gravAcceleration";
            this.gravAcceleration.Size = new System.Drawing.Size(47, 20);
            this.gravAcceleration.TabIndex = 9;
            this.gravAcceleration.KeyUp += new System.Windows.Forms.KeyEventHandler(this.softwareSettingFieldChanged_KeyUp);
            // 
            // srg
            // 
            this.srg.Enabled = false;
            this.srg.Location = new System.Drawing.Point(337, 159);
            this.srg.Name = "srg";
            this.srg.Size = new System.Drawing.Size(44, 20);
            this.srg.TabIndex = 4;
            this.srg.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deviceSettings_KeyDown);
            this.srg.KeyUp += new System.Windows.Forms.KeyEventHandler(this.deviceSettingFieldChanged_KeyUp);
            // 
            // sla
            // 
            this.sla.Enabled = false;
            this.sla.Location = new System.Drawing.Point(337, 257);
            this.sla.Name = "sla";
            this.sla.Size = new System.Drawing.Size(44, 20);
            this.sla.TabIndex = 7;
            this.sla.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deviceSettings_KeyDown);
            this.sla.KeyUp += new System.Windows.Forms.KeyEventHandler(this.deviceSettingFieldChanged_KeyUp);
            // 
            // sli
            // 
            this.sli.Enabled = false;
            this.sli.Location = new System.Drawing.Point(337, 229);
            this.sli.Name = "sli";
            this.sli.Size = new System.Drawing.Size(44, 20);
            this.sli.TabIndex = 6;
            this.sli.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deviceSettings_KeyDown);
            this.sli.KeyUp += new System.Windows.Forms.KeyEventHandler(this.deviceSettingFieldChanged_KeyUp);
            // 
            // slo
            // 
            this.slo.Enabled = false;
            this.slo.Location = new System.Drawing.Point(337, 197);
            this.slo.Name = "slo";
            this.slo.Size = new System.Drawing.Size(44, 20);
            this.slo.TabIndex = 5;
            this.slo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deviceSettings_KeyDown);
            this.slo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.deviceSettingFieldChanged_KeyUp);
            // 
            // sr6
            // 
            this.sr6.Enabled = false;
            this.sr6.Location = new System.Drawing.Point(337, 128);
            this.sr6.Name = "sr6";
            this.sr6.Size = new System.Drawing.Size(44, 20);
            this.sr6.TabIndex = 3;
            this.sr6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deviceSettings_KeyDown);
            this.sr6.KeyUp += new System.Windows.Forms.KeyEventHandler(this.deviceSettingFieldChanged_KeyUp);
            // 
            // sr5
            // 
            this.sr5.Enabled = false;
            this.sr5.Location = new System.Drawing.Point(337, 98);
            this.sr5.Name = "sr5";
            this.sr5.Size = new System.Drawing.Size(44, 20);
            this.sr5.TabIndex = 2;
            this.sr5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deviceSettings_KeyDown);
            this.sr5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.deviceSettingFieldChanged_KeyUp);
            // 
            // stl
            // 
            this.stl.Enabled = false;
            this.stl.Location = new System.Drawing.Point(337, 65);
            this.stl.Name = "stl";
            this.stl.Size = new System.Drawing.Size(44, 20);
            this.stl.TabIndex = 1;
            this.stl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deviceSettings_KeyDown);
            this.stl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.deviceSettingFieldChanged_KeyUp);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(491, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Erdbeschleunigung:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(664, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "ms";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(491, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Übertragungs Intervall:";
            // 
            // saveDeviceConfig
            // 
            this.saveDeviceConfig.Location = new System.Drawing.Point(162, 300);
            this.saveDeviceConfig.Name = "saveDeviceConfig";
            this.saveDeviceConfig.Size = new System.Drawing.Size(146, 23);
            this.saveDeviceConfig.TabIndex = 10;
            this.saveDeviceConfig.Text = "Speichern";
            this.saveDeviceConfig.UseVisualStyleBackColor = true;
            this.saveDeviceConfig.Click += new System.EventHandler(this.saveDeviceConfig_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(387, 162);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(16, 13);
            this.label25.TabIndex = 20;
            this.label25.Text = "Ω";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(98, 162);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(229, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = "Widerstandswert RG des Messkraftverstärkers:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(387, 260);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 13);
            this.label23.TabIndex = 17;
            this.label23.Text = "kg";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(98, 260);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(212, 13);
            this.label24.TabIndex = 15;
            this.label24.Text = "Messbereichs-Maximum der Kraftmessdose:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(387, 232);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 13);
            this.label21.TabIndex = 14;
            this.label21.Text = "kg";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(98, 232);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(209, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Messbereichs-Minimum der Kraftmessdose:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(387, 200);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "mV / V";
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(98, 185);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(226, 32);
            this.label20.TabIndex = 9;
            this.label20.Text = "Differenzspannung der Kraftmessdose pro Volt Versorgungsspannung:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(387, 131);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Ω";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(98, 131);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(210, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Widerstandswert R6 des Spannungsteilers:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(387, 101);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Ω";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(98, 101);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(210, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Widerstandswert R5 des Spannungsteilers:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(387, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "mm";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(98, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(206, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Schubstangenlänge des Wegaufnehmers:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(791, 389);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Messung";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ohneNullpunkt);
            this.groupBox4.Controls.Add(this.wegNull);
            this.groupBox4.Controls.Add(this.kraftNull);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.federkonstanteNull);
            this.groupBox4.Controls.Add(this.filtern);
            this.groupBox4.Location = new System.Drawing.Point(574, 196);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(208, 187);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filtern";
            // 
            // ohneNullpunkt
            // 
            this.ohneNullpunkt.AutoSize = true;
            this.ohneNullpunkt.Location = new System.Drawing.Point(11, 122);
            this.ohneNullpunkt.Name = "ohneNullpunkt";
            this.ohneNullpunkt.Size = new System.Drawing.Size(100, 17);
            this.ohneNullpunkt.TabIndex = 38;
            this.ohneNullpunkt.Text = "Ohne Nullpunkt";
            this.ohneNullpunkt.UseVisualStyleBackColor = true;
            // 
            // wegNull
            // 
            this.wegNull.AutoSize = true;
            this.wegNull.Location = new System.Drawing.Point(11, 99);
            this.wegNull.Name = "wegNull";
            this.wegNull.Size = new System.Drawing.Size(67, 17);
            this.wegNull.TabIndex = 37;
            this.wegNull.Text = "Weg = 0";
            this.wegNull.UseVisualStyleBackColor = true;
            // 
            // kraftNull
            // 
            this.kraftNull.AutoSize = true;
            this.kraftNull.Location = new System.Drawing.Point(11, 76);
            this.kraftNull.Name = "kraftNull";
            this.kraftNull.Size = new System.Drawing.Size(66, 17);
            this.kraftNull.TabIndex = 36;
            this.kraftNull.Text = "Kraft = 0";
            this.kraftNull.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(6, 18);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(202, 32);
            this.label29.TabIndex = 35;
            this.label29.Text = "Datensätze mit folgenden Eigenschaften unwiederruflich löschen:";
            // 
            // federkonstanteNull
            // 
            this.federkonstanteNull.AutoSize = true;
            this.federkonstanteNull.Location = new System.Drawing.Point(11, 53);
            this.federkonstanteNull.Name = "federkonstanteNull";
            this.federkonstanteNull.Size = new System.Drawing.Size(121, 17);
            this.federkonstanteNull.TabIndex = 34;
            this.federkonstanteNull.Text = "Ferderkonstante = 0";
            this.federkonstanteNull.UseVisualStyleBackColor = true;
            // 
            // filtern
            // 
            this.filtern.Location = new System.Drawing.Point(127, 158);
            this.filtern.Name = "filtern";
            this.filtern.Size = new System.Drawing.Size(75, 23);
            this.filtern.TabIndex = 33;
            this.filtern.Text = "Anwenden";
            this.filtern.UseVisualStyleBackColor = true;
            this.filtern.Click += new System.EventHandler(this.filtern_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.dataFormat);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(328, 196);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(240, 187);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Speichern";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 63);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(71, 13);
            this.label32.TabIndex = 36;
            this.label32.Text = "Federnummer";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(93, 60);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(141, 20);
            this.textBox1.TabIndex = 35;
            // 
            // dataFormat
            // 
            this.dataFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataFormat.FormattingEnabled = true;
            this.dataFormat.Items.AddRange(new object[] {
            "Microsoft Excel (*.xls)"});
            this.dataFormat.Location = new System.Drawing.Point(93, 29);
            this.dataFormat.Name = "dataFormat";
            this.dataFormat.Size = new System.Drawing.Size(141, 21);
            this.dataFormat.TabIndex = 34;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(7, 32);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 13);
            this.label28.TabIndex = 33;
            this.label28.Text = "Dateiformat";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(132, 158);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "Daten Speichern";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ldf_nr,
            this.date,
            this.force,
            this.distance,
            this.springConst,
            this.tara,
            this.nullpunktInfo});
            this.dataGridView1.Location = new System.Drawing.Point(7, 7);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(775, 183);
            this.dataGridView1.TabIndex = 31;
            // 
            // ldf_nr
            // 
            this.ldf_nr.Frozen = true;
            this.ldf_nr.HeaderText = "lfd. Nr.";
            this.ldf_nr.Name = "ldf_nr";
            this.ldf_nr.ReadOnly = true;
            // 
            // date
            // 
            this.date.Frozen = true;
            this.date.HeaderText = "Datum / Uhrzeit";
            this.date.Name = "date";
            this.date.ReadOnly = true;
            this.date.Width = 160;
            // 
            // force
            // 
            this.force.Frozen = true;
            this.force.HeaderText = "Kraft in kg";
            this.force.Name = "force";
            this.force.ReadOnly = true;
            this.force.Width = 110;
            // 
            // distance
            // 
            this.distance.Frozen = true;
            this.distance.HeaderText = "Weg in mm";
            this.distance.Name = "distance";
            this.distance.ReadOnly = true;
            // 
            // springConst
            // 
            this.springConst.Frozen = true;
            this.springConst.HeaderText = "Federkonstante in N/mm";
            this.springConst.Name = "springConst";
            this.springConst.ReadOnly = true;
            this.springConst.Width = 150;
            // 
            // tara
            // 
            this.tara.Frozen = true;
            this.tara.HeaderText = "Nullpunkt gesetzt?";
            this.tara.Name = "tara";
            this.tara.ReadOnly = true;
            this.tara.Width = 120;
            // 
            // nullpunktInfo
            // 
            this.nullpunktInfo.Frozen = true;
            this.nullpunktInfo.HeaderText = "nullpunktInfo";
            this.nullpunktInfo.Name = "nullpunktInfo";
            this.nullpunktInfo.ReadOnly = true;
            this.nullpunktInfo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nullpunktInfo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.nullpunktInfo.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.setTaraInfoPic);
            this.groupBox2.Controls.Add(this.nullpunktSetzen);
            this.groupBox2.Controls.Add(this.measurmentInfo);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.federkonstante);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.uebertragungStart);
            this.groupBox2.Controls.Add(this.measuredForce);
            this.groupBox2.Controls.Add(this.measuredDistance);
            this.groupBox2.Location = new System.Drawing.Point(6, 196);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(316, 187);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Messwerte";
            // 
            // setTaraInfoPic
            // 
            this.setTaraInfoPic.Cursor = System.Windows.Forms.Cursors.Default;
            this.setTaraInfoPic.Image = global::Automatischer_Federkraftmesser.Properties.Resources.loading;
            this.setTaraInfoPic.Location = new System.Drawing.Point(113, 158);
            this.setTaraInfoPic.Name = "setTaraInfoPic";
            this.setTaraInfoPic.Size = new System.Drawing.Size(23, 23);
            this.setTaraInfoPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.setTaraInfoPic.TabIndex = 7;
            this.setTaraInfoPic.TabStop = false;
            this.setTaraInfoPic.Visible = false;
            // 
            // nullpunktSetzen
            // 
            this.nullpunktSetzen.Enabled = false;
            this.nullpunktSetzen.Location = new System.Drawing.Point(6, 158);
            this.nullpunktSetzen.Name = "nullpunktSetzen";
            this.nullpunktSetzen.Size = new System.Drawing.Size(101, 23);
            this.nullpunktSetzen.TabIndex = 13;
            this.nullpunktSetzen.Text = "Nullpunkt setzen";
            this.nullpunktSetzen.UseVisualStyleBackColor = true;
            this.nullpunktSetzen.Click += new System.EventHandler(this.nullpunktSetzen_Click);
            // 
            // measurmentInfo
            // 
            this.measurmentInfo.Location = new System.Drawing.Point(98, 107);
            this.measurmentInfo.Name = "measurmentInfo";
            this.measurmentInfo.Size = new System.Drawing.Size(212, 48);
            this.measurmentInfo.TabIndex = 12;
            this.measurmentInfo.Text = "-";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 107);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 13);
            this.label27.TabIndex = 11;
            this.label27.Text = "Hinweis:";
            // 
            // federkonstante
            // 
            this.federkonstante.AutoSize = true;
            this.federkonstante.Location = new System.Drawing.Point(98, 86);
            this.federkonstante.Name = "federkonstante";
            this.federkonstante.Size = new System.Drawing.Size(10, 13);
            this.federkonstante.TabIndex = 10;
            this.federkonstante.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Federkonstante:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Kraft:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Weg:";
            // 
            // uebertragungStart
            // 
            this.uebertragungStart.Enabled = false;
            this.uebertragungStart.Location = new System.Drawing.Point(186, 158);
            this.uebertragungStart.Name = "uebertragungStart";
            this.uebertragungStart.Size = new System.Drawing.Size(124, 23);
            this.uebertragungStart.TabIndex = 6;
            this.uebertragungStart.Text = "Übertragung starten";
            this.uebertragungStart.UseVisualStyleBackColor = true;
            this.uebertragungStart.Click += new System.EventHandler(this.button2_Click);
            // 
            // measuredForce
            // 
            this.measuredForce.AutoSize = true;
            this.measuredForce.Location = new System.Drawing.Point(98, 60);
            this.measuredForce.Name = "measuredForce";
            this.measuredForce.Size = new System.Drawing.Size(10, 13);
            this.measuredForce.TabIndex = 5;
            this.measuredForce.Text = "-";
            // 
            // measuredDistance
            // 
            this.measuredDistance.AutoSize = true;
            this.measuredDistance.Location = new System.Drawing.Point(98, 33);
            this.measuredDistance.Name = "measuredDistance";
            this.measuredDistance.Size = new System.Drawing.Size(10, 13);
            this.measuredDistance.TabIndex = 4;
            this.measuredDistance.Text = "-";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.verbindenButton);
            this.groupBox1.Controls.Add(this.deviceStatus);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.firmwareInfo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.verbindungstyp);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.hardwareInfo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(584, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(225, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Geräteinformationen";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.comboBox1.Location = new System.Drawing.Point(91, 93);
            this.comboBox1.MaxDropDownItems = 100;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(63, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(10, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(208, 2);
            this.label6.TabIndex = 6;
            // 
            // verbindenButton
            // 
            this.verbindenButton.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verbindenButton.ForeColor = System.Drawing.Color.Red;
            this.verbindenButton.Location = new System.Drawing.Point(10, 92);
            this.verbindenButton.Name = "verbindenButton";
            this.verbindenButton.Size = new System.Drawing.Size(75, 23);
            this.verbindenButton.TabIndex = 1;
            this.verbindenButton.Text = "Verbinden";
            this.verbindenButton.UseVisualStyleBackColor = true;
            this.verbindenButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // deviceStatus
            // 
            this.deviceStatus.AutoSize = true;
            this.deviceStatus.Location = new System.Drawing.Point(108, 65);
            this.deviceStatus.Name = "deviceStatus";
            this.deviceStatus.Size = new System.Drawing.Size(46, 13);
            this.deviceStatus.TabIndex = 8;
            this.deviceStatus.Text = "getrennt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Status:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(202, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // firmwareInfo
            // 
            this.firmwareInfo.AutoSize = true;
            this.firmwareInfo.Location = new System.Drawing.Point(108, 35);
            this.firmwareInfo.Name = "firmwareInfo";
            this.firmwareInfo.Size = new System.Drawing.Size(10, 13);
            this.firmwareInfo.TabIndex = 5;
            this.firmwareInfo.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Firmware Version:";
            // 
            // verbindungstyp
            // 
            this.verbindungstyp.AutoSize = true;
            this.verbindungstyp.Location = new System.Drawing.Point(108, 50);
            this.verbindungstyp.Name = "verbindungstyp";
            this.verbindungstyp.Size = new System.Drawing.Size(10, 13);
            this.verbindungstyp.TabIndex = 3;
            this.verbindungstyp.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Verbindung:";
            // 
            // hardwareInfo
            // 
            this.hardwareInfo.AutoSize = true;
            this.hardwareInfo.Location = new System.Drawing.Point(108, 20);
            this.hardwareInfo.Name = "hardwareInfo";
            this.hardwareInfo.Size = new System.Drawing.Size(10, 13);
            this.hardwareInfo.TabIndex = 1;
            this.hardwareInfo.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hardware Version:";
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Location = new System.Drawing.Point(12, 131);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(799, 415);
            this.tabControl.TabIndex = 0;
            this.tabControl.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl_Selecting);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Automatischer_Federkraftmesser.Properties.Resources.ps_fahrwerke_logo;
            this.pictureBox2.Location = new System.Drawing.Point(12, 16);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(243, 99);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(823, 558);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automatischer Federkraftmesser - Auswertungssoftware (PS-Fahrwerke Durlangen) v1." +
                "0.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveConfigInfoPic)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setTaraInfoPic)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox transInterval;
        private System.Windows.Forms.TextBox gravAcceleration;
        private System.Windows.Forms.TextBox srg;
        private System.Windows.Forms.TextBox sla;
        private System.Windows.Forms.TextBox sli;
        private System.Windows.Forms.TextBox slo;
        private System.Windows.Forms.TextBox sr6;
        private System.Windows.Forms.TextBox sr5;
        private System.Windows.Forms.TextBox stl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button saveDeviceConfig;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label federkonstante;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button uebertragungStart;
        private System.Windows.Forms.Label measuredForce;
        private System.Windows.Forms.Label measuredDistance;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button verbindenButton;
        private System.Windows.Forms.Label deviceStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label firmwareInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label verbindungstyp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label hardwareInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Label measurmentInfo;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox saveConfigInfoPic;
        private System.Windows.Forms.Button nullpunktSetzen;
        private System.Windows.Forms.PictureBox setTaraInfoPic;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button filtern;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox dataFormat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox ohneNullpunkt;
        private System.Windows.Forms.CheckBox wegNull;
        private System.Windows.Forms.CheckBox kraftNull;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox federkonstanteNull;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ldf_nr;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn force;
        private System.Windows.Forms.DataGridViewTextBoxColumn distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn springConst;
        private System.Windows.Forms.DataGridViewImageColumn tara;
        private System.Windows.Forms.DataGridViewTextBoxColumn nullpunktInfo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

