﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
//DEBUG: MESSAGEBOX etc.
using System.Windows.Forms;

namespace Automatischer_Federkraftmesser
{
    static class rs232
    {
        static public bool _boBusy;
        static private SerialPort _spPort;
        static private string _strCommand;
        static public string _strResponse = "";
        static public bool _boAbortThread = false;

        /// <summary>SerialPort Verbindung herstellen oder trennen
        /// <para>cStauts: 0 -> Trennen, 1 -> Verbinden</para>
        /// </summary>
        static public int SerialPortChangeStatus(SerialPort port, int cStatus)
        {
            switch (cStatus)
            {
                case 0:
                    if (port.IsOpen)
                    {
                        try
                        {
                            port.Close();
                            return 0;
                        }
                        catch
                        {
                            return 1;
                        }
                    }
                    else
                        return 2;

                case 1:
                    if (!port.IsOpen)
                    {
                        try
                        {
                            port.Open();
                            return 0;
                        }
                        catch
                        {
                            return 1;
                        }
                    }
                    else
                        return 2;
            }
            return 3;
        }

        static public void SendCommand(SerialPort port, string strCommand)
        {
            _boBusy = true;
            _spPort = port;
            _strCommand = strCommand;
            Thread thread = new Thread(new ThreadStart(SendCommand_Thread));
            thread.Start();
        }

        static private void SendCommand_Thread()
        {
            if (_spPort.IsOpen)
                _spPort.Write(_strCommand);

            bool boResponseReceived = false;
            string strReceived = "";
            int iTimeoutCount = 20; //Timeout 2sec.
            do
            {
                if (strReceived.IndexOf("err") != -1)
                    boResponseReceived = true;
                Thread.Sleep(100);
                
                if(_spPort.IsOpen)
                    strReceived += _spPort.ReadExisting();
                iTimeoutCount--;
            } while (!boResponseReceived && iTimeoutCount > 0);
            if (!_boAbortThread)
            {
                if (boResponseReceived)
                    _strResponse = strReceived;
                else
                    _strResponse = "err 199";
            }
            _boAbortThread = false;
            _boBusy = false;
        }
    }
}
