// Ansteuerung eines HD44780 kompatiblen LCD im 4-Bit-Interfacemodus
// http://www.mikrocontroller.net/articles/AVR-GCC-Tutorial
//
void lcd_data(unsigned char temp1);
void lcd_puts(char *data);
void lcd_command(unsigned char temp1);
void lcd_enable(void);
void lcd_init(void);
void lcd_home(void);
void lcd_clear(void);
void lcd_cursor(uint8_t x, uint8_t y);

extern void lcd_puts_p(const char *s );
#define lcd_puts_P(__s)       lcd_puts_p(PSTR(__s))

// LCD Befehle
 
#define CLEAR_DISPLAY 0x01
#define CURSOR_HOME   0x02
 
// Pinbelegung f�r das LCD, an verwendete Pins anpassen
 
#define LCD_PORT      PORTB
#define LCD_DDR       DDRB
#define LCD_RS        PB4
#define LCD_EN        PB5
// D4 bis D7 des LCD sind mit PB0 bis PB3 des AVR verbunden 