/*
####################################################################################################
Projektname:			Federkonstanten-Messger�t	

Projektbeschreibung:   	

Autor:					Philip Urban 

Bearbeitungszeitraum:	 bis  

Projekt-ID:				

Mikrocontroller:		atmega8
SYSTEMTAKT:				8000000Hz

####################################################################################################
*/


#include <util/delay.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#include "lcd.h"
#include "uart.h"

#ifndef EEMEM
#define EEMEM  __attribute__ ((section (".eeprom")))
#endif

#define UART_BAUD_RATE  9600
#define TASTER1 !(PIND & (1<<PIND2))
#define TASTER2 !(PIND & (1<<PIND3))
#define ANZAHL_TASTER 2
#define NOTSET 0xFFFF

//Versionsinformation
#define HARDWARE_VERSION "rhi 100811;"
#define FIRMWARE_VERSION "rfi 190811;"
//Anzahl Befehle
#define COMMAND_COUNT 12
//Fehlermeldung senden
#define ERROR_RESPONSE(error) SEND_RESPONSE(err error;)
#define SEND_RESPONSE(errorMsg) uart_puts_P(#errorMsg)
//Fehlercodes
#define SUCCESSFUL 100
#define RECEIVED_NO_VALUE 101
#define FORCE_TOO_LOW 102
#define TARA_NOT_SET 103
//Minimale Kraft die aufgewendet sein muss in Prozent von Datenblatt-Minimum (Angabe in 1.XX)
#define MINIMUM_FORCE 1.5

typedef unsigned int 	uint;
typedef unsigned char 	uchar;

uchar show = 0;

//Entprellroutine
volatile uint milliseconds = 0;
volatile uchar Taster[ANZAHL_TASTER];
uchar pressed_from_zero[ANZAHL_TASTER]={1,1};
volatile uchar press_counter[ANZAHL_TASTER]={0,0};
volatile uchar key_pressed[ANZAHL_TASTER];
volatile uchar key_control[ANZAHL_TASTER];

//Messung
float force_tara = 0;
float distance_tara = 0;
uchar tara_set = 0;

//UART / Kommunikationsprotokoll
uint c;
uchar startInstructionReceive = 1;
uchar instructionCount = 0;
uchar valueCount = 0;
char receivedInstruction[4];
char strTemp_ReceivedValue[6];
uint receivedValue = NOTSET;
uchar csr = 0;
struct instructionBundle 
	{
		char instruction[4];
		void (*function)(void);
	};
	
//EEPROM Konfigurationsparameter
uint eeprom_config_TransducorLength EEMEM;
uint eeprom_config_R5 EEMEM;
uint eeprom_config_R6 EEMEM;
uint eeprom_config_LoadcellOutput EEMEM;
uint eeprom_config_RG EEMEM;
uint eeprom_config_LoadcellMinimum EEMEM;
uint eeprom_config_LoadcellMaximum EEMEM;

//Lokale Tempor�re Konfigurationsparameter
uint local_config_TransducorLength;
uint local_config_R5;
uint local_config_R6;
uint local_config_LoadcellOutput;
uint local_config_RG;
uint local_config_LoadcellMinimum;
uint local_config_LoadcellMaximum;

void long_delay(uint16_t ms) 
{
    for(; ms>0; ms--) _delay_ms(1);
}


uint16_t ReadChannel(uint8_t mux)
{
  uint8_t i;
  uint16_t result;
  int i2 = 10;                    		//Anzahl Wandlungen zur Mittelwertbildung
  ADMUX = mux;                        	//Kanal           
  ADMUX  |= (1<<REFS0);         		//Referenzspannung
  ADCSRA |= (1<<ADEN);   				//ADC Enable
  
  ADCSRA |= (1<<ADPS2) | (1<<ADPS1); 	//Vorteiler 64
  ADCSRA &= ~(1<<ADPS0);
  
  ADCSRA |= (1<<ADSC);                  
  while ( ADCSRA & (1<<ADSC) ) {
     ;
  }
  result = ADCW;
  result = 0; 
 
 
  for( i=0; i<i2; i++ )
  {
    ADCSRA |= (1<<ADSC); 
    while ( ADCSRA & (1<<ADSC) ) {
      ;
    }
    result += ADCW;
  }
  ADCSRA &= ~(1<<ADEN);
  result /= i2;
  return result;
}



void lcd_puti(int var)
{
	char Buffer[7];
	itoa(var,Buffer,10);
	lcd_puts(Buffer);
}

void lcd_putui(uint var)
{
	char Buffer[7];
	utoa(var,Buffer,10);
	lcd_puts(Buffer);
}

void lcd_putf(float var, int nks)
{
	unsigned char vks = 0;
	if(var < 10)
		vks = 1;
	else if(var < 100)
		vks = 2;
	else if(var < 1000)
		vks = 3;
	else if(var < 10000)
		vks = 4;
	vks += nks;
		
    char Buffer[7];
	dtostrf(var,vks,nks,Buffer);
	lcd_puts(Buffer);
}

void uart_putui(unsigned int var)
{
    char Buffer[7];
	utoa(var, Buffer, 10);
	uart_puts( Buffer );
}

ISR (TIMER2_COMP_vect)
{
	
	/*ENTPRELLROUTINE f�r alle Schalter/Taster im Taster-Array (Zustands�nderungen werden ausgewertet)*/

	for(uchar i=0;i<ANZAHL_TASTER;i++)
	{
	
		if(Taster[i] && press_counter[i]==0 && pressed_from_zero[i]==1)
		{	//Wenn Taste gedr�ckt, Sperrcounter==0 und Taster davor losgelassen war
			press_counter[i]=50; 											//press_counter aufladen (Sperrzeit)

				//TASTER[i] gedr�ckt und entprellt: Eintrag im Pressarray wird dementsprechend ge�ndert
				key_pressed[i]=1;
				
				pressed_from_zero[i]= 0;

		} 
		else if(!Taster[i] && press_counter[i]==0) 
			pressed_from_zero[i] = 1; //Wenn der Taster losgelassen ist und der Sperrcounter leer ist, dann ist der Taster beim loslassen entprellt
		
		if(press_counter[i]>0)
			press_counter[i]--; //Sperrcounter runterz�hlen
	
	}
	
	
	if(milliseconds <= 1000)
		milliseconds++;
	else
	{
		milliseconds = 0;
		PORTD ^= (1<<PD6);
	}
	
}



void lcd_printMsg(uchar msg)
{
	lcd_clear();
	switch(msg)
	{
	case 0:
			lcd_cursor(0,1);
			lcd_puts_P("Kraft zu gering!");
			lcd_cursor(0,2);
			lcd_puts_P("  (Code 102)  ");
		break;
	case 1:
			lcd_cursor(0,1);
			lcd_puts_P("Kein Nullpunkt!");
			lcd_cursor(0,2);
			lcd_puts_P("  (Code 103)  ");
		break;
	}
	long_delay(1500);
	lcd_clear();
}
uchar CheckMinimumForce(float force)
{
	if(force>(float)local_config_LoadcellMinimum*MINIMUM_FORCE)
		return 0;
	else
		return 1;
}
float GetCurrentForce(void)
{
	//SEHR RAM INTENSIV!
	float voltageDividerU = ((float)local_config_R5+(float)local_config_R6)/(float)local_config_R6;
	float loadcellVoltage = (0.0048876)*ReadChannel(2)*voltageDividerU;
	float voltagePerKg = (((float)local_config_LoadcellOutput/1000.0)*loadcellVoltage)/(float)local_config_LoadcellMaximum;
	float measuringAmplifierU = 4.0+(60000.0/(float)local_config_RG);
	float measuredVoltage = ((0.0048876)*ReadChannel(1))/measuringAmplifierU;
	//float force_current = measuredVoltage/voltagePerKg;
	return measuredVoltage/voltagePerKg;
}

float GetCurrentDistance(void)
{
	return ((float)local_config_TransducorLength/1023.0)*(ReadChannel(0));
}

float GetDeltaForce(void)
{
	float force_current = GetCurrentForce();
	if(CheckMinimumForce(force_current)==1)
		return 1.2345678;
	else
		return force_current - force_tara;	
}

float GetDeltaDistance(void)
{
	float distance_current = GetCurrentDistance();
	return distance_current - distance_tara;
}

void SetConfigParameter(volatile uint *localConfig, uint *eepromConfig, uint value)
{
	if(value!=NOTSET)
	{
		*localConfig = value;
		eeprom_write_word(eepromConfig, *localConfig);
		ERROR_RESPONSE(SUCCESSFUL);
	}
	else
		ERROR_RESPONSE(RECEIVED_NO_VALUE);
}

//Schubstangenl�nge des Wegaufnehmers einstellen (mm)
void stl(void)
{
	SetConfigParameter(&local_config_TransducorLength, &eeprom_config_TransducorLength, receivedValue);
}

//Widerstandswert R5 des Spannungsteilers einstellen (Ohm)
void sr5(void)
{
	SetConfigParameter(&local_config_R5, &eeprom_config_R5, receivedValue); 
}

//Widerstandswert R5 des Spannungsteilers einstellen (Ohm)
void sr6(void)
{
	SetConfigParameter(&local_config_R6, &eeprom_config_R6, receivedValue);
}

//Spannungsbereich der Kraftmessdose (mV/V) einstellen (mV)
void slo(void)
{
	SetConfigParameter(&local_config_LoadcellOutput, &eeprom_config_LoadcellOutput, receivedValue);
}

//Widerstandswert RG des Messwertverst�rkers einstellen (Ohm)
void srg(void)
{
	SetConfigParameter(&local_config_RG, &eeprom_config_RG, receivedValue);
}

//Messbereichs-Minimum der Kraftmessdose einstellen (kg)
void sli(void)
{
	SetConfigParameter(&local_config_LoadcellMinimum, &eeprom_config_LoadcellMinimum, receivedValue);
}

//Messbereichs-Maximum der Kraftmessdose einstellen (kg)
void sla(void)
{
	SetConfigParameter(&local_config_LoadcellMaximum, &eeprom_config_LoadcellMaximum, receivedValue);
}

//Messausgangslage setzen (Tara/Nullen)
void svz(void)
{
	float force = GetCurrentForce();
	float distance = GetCurrentDistance();
	if(CheckMinimumForce(force)==0)
	{
		force_tara = force; 
		distance_tara = distance;
		tara_set = 1;
		ERROR_RESPONSE(SUCCESSFUL);
	}
	else
		ERROR_RESPONSE(FORCE_TOO_LOW);
}

//Versionsnummer �bertragen
void rhi(void)
{
	uart_puts_P(HARDWARE_VERSION);
	ERROR_RESPONSE(SUCCESSFUL);
}
//Versionsnummer �bertragen
void rfi(void)
{
	uart_puts_P(FIRMWARE_VERSION);
	ERROR_RESPONSE(SUCCESSFUL);
}

//Messwert �bertragen: Weg in mm
void rmd(void)
{
/*
	float distance_delta = GetDeltaDistance();
	
	uart_puts_P("rmd ");
	uart_putf(distance_delta, 2);
	uart_putc(';');
*/
	uart_puts_P("rmd ");
	uart_putf(GetCurrentDistance(), 2);
	if(tara_set == 0)
		ERROR_RESPONSE(TARA_NOT_SET);
	else
		ERROR_RESPONSE(SUCCESSFUL);
}

//Messwert �bertragen: Kraft in kg
void rmf(void)
{
	float force_current = GetCurrentForce();
	if(CheckMinimumForce(force_current)==1)
		ERROR_RESPONSE(FORCE_TOO_LOW);
	else
	{
		uart_puts_P("rmf ");
		uart_putf(force_current, 2);
		uart_putc(';');
		if(tara_set == 0)
			ERROR_RESPONSE(TARA_NOT_SET);
		else
			ERROR_RESPONSE(SUCCESSFUL);
	}
/*
	
	float force_delta = GetDeltaForce();
	if(force_delta==1.2345678)
		ERROR_RESPONSE(FORCE_TOO_LOW);
	else
	{
		uart_puts_P("rmf ");
		uart_putf(force_delta, 2);
		uart_putc(';');
		if(tara_set == 0)
			ERROR_RESPONSE(TARA_NOT_SET);
		else
			ERROR_RESPONSE(SUCCESSFUL);

	}
*/
}


void InitConfiguration(void)
{
	local_config_TransducorLength = eeprom_read_word(&eeprom_config_TransducorLength);
	local_config_R5 = eeprom_read_word(&eeprom_config_R5);
	local_config_R6 = eeprom_read_word(&eeprom_config_R6);
	local_config_LoadcellOutput = eeprom_read_word(&eeprom_config_LoadcellOutput);
	local_config_RG = eeprom_read_word(&eeprom_config_RG);
	local_config_LoadcellMinimum = eeprom_read_word(&eeprom_config_LoadcellMinimum);
	local_config_LoadcellMaximum = eeprom_read_word(&eeprom_config_LoadcellMaximum);
}

int main(void)
{
	//Timer Initialisieren - 1ms (1kHz)
	TCCR2 |= (1<<WGM21); //CTC Mode
	TCCR2 |= (1<<CS22); //Prescaler 64 / OCR0 125: 8MHz:64:125 = 1KHz
	OCR2 = 125;
	TIMSK |= (1<<OCIE2);
	
	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
	lcd_init();
	sei();
	
	//Befehlsliste festlegen
		struct instructionBundle instructionList[COMMAND_COUNT] =
	{
		{ "stl", stl},
		{ "sr5", sr5},
		{ "sr6", sr6},
		{ "slo", slo},
		{ "srg", srg},
		{ "sli", sli},
		{ "sla", sla},
		{ "svz", svz},
		{ "thi", rhi},
		{ "tfi", rfi},
		{ "tmd", rmd},
		{ "tmf", rmf}

	};
	
	/*Portkonfiguration*/
	DDRD |= (1<<DDD6);
	DDRD &= ~((1<<DDD2)|(1<<DDD3));
	PORTD |= (1<<PD2)|(1<<PD3);
	DDRC &= ~((1<<DDC0)|(1<<DDC1)|(1<<DDC2));
	//PORTC |= (1<<PC0)|(1<<PC2);
	
	//EEPROM Daten Initialisieren
	InitConfiguration();
	
	lcd_clear();

	for(;;)
	{
		//Der Zustand der einzelnen Schalter/Taster wird st�ndig an das Array �bergeben
		Taster[0] = TASTER1;
		Taster[1] = TASTER2;

		//Wenn Taster gedr�ckt war und nun losgelassen wurde, wird der Sperrcounter nochmals geladen um das Prellen beim loslassen zu ignorieren
		for(uchar i=0;i<6;i++)
		{
			if(!Taster[i] && key_control[i]==1)
			{ 
					press_counter[i]=50;
					key_control[i] = 0;
			}
			
		}
		
		if(key_pressed[1]==1)
		{
			float force = GetCurrentForce();
			float distance = GetCurrentDistance();
			if(CheckMinimumForce(force)==0)
			{
				force_tara = force; 
				distance_tara = distance;
				tara_set = 1;
				
				lcd_cursor(0,1);
				lcd_puts_P(" Ger");
				lcd_data(0b11100001); //�
				lcd_puts_P("t genullt! ");
				lcd_cursor(0,2);
				lcd_puts_P("  (Code 100)  ");
				long_delay(1500);
				lcd_clear();
			}
			else
			{
				lcd_printMsg(0);
			}
			
			key_pressed[1]=0;
			
		}
		
		if(key_pressed[0]==1)
		{
			if(tara_set==0)
				lcd_printMsg(1);
			else
			{
				force_tara = 0;
				distance_tara = 0;
				tara_set = 0;
				lcd_cursor(0,1);
				lcd_puts_P(" Tara gel");
				lcd_data(0b11101111); //�
				lcd_puts_P("scht! ");
				lcd_cursor(0,2);
				lcd_puts_P("  (Code 104)  ");
				long_delay(1500);
				lcd_clear();
			}
			key_pressed[0]=0;
		}
		
		if(show == 1)
		{
			float distance_delta = GetDeltaDistance();
			float force_delta = GetDeltaForce();
			
			if(force_delta==1.2345678)
			{
				lcd_printMsg(0);
			}
			else
			{	
				lcd_clear();
				lcd_cursor(0,1);
				lcd_putf(force_delta,1);
				lcd_puts_P("kg  ");
				lcd_putf(distance_delta,1);
				lcd_puts_P("mm");
				
				lcd_cursor(0,2);
				lcd_puts_P("c: ");
				if(distance_delta!=0)
					lcd_putf((force_delta/distance_delta)*9.81,1);
				else
					lcd_puts_P("0");
				lcd_puts_P("N/mm");
			}
		}
		
		if(milliseconds == 1000)
			show = 1;
		else
			show = 0;
		
        c = uart_getc();
        if ( c & UART_NO_DATA )
        {
        }
        else
        {			
			
			if(csr != 1)
			{
				//Befehle Empfangen BEGINN
				if(startInstructionReceive == 1)
				{
					if(c != ';' && c != ' ' && c != '\n' && c != '\r')
					{
						receivedInstruction[instructionCount] = (char)c;
						instructionCount++;
					}
					else
					{
						receivedInstruction[instructionCount] = '\0';
						startInstructionReceive = 0;
						instructionCount = 0;
						if(c == ';')
							csr = 1;
					}
				}
				else
				{
					if(c != ';' && c != ' ')
					{
						strTemp_ReceivedValue[valueCount] = (char)c;
						valueCount++;
					}
					else
					{
						if(valueCount != 0)
						{
							strTemp_ReceivedValue[valueCount] = '\0';
							receivedValue = atoi(strTemp_ReceivedValue);
							valueCount = 0;
						}
						csr = 1;
					}
				}
				//Befehle Empfangen ENDE
			}
			
			if(csr == 1)
			{
				//Befehle Verarbeiten BEGINN
				//CSR R�ckmeldung BEGINN
				uart_puts_P("csr");
				uart_putc(' ');
				uart_putui(receivedValue);
				uart_putc(';');
				//CSR R�ckmeldung ENDE
				
				for(uchar i = 0; i<COMMAND_COUNT; i++)
				{	
					if(strcmp(instructionList[i].instruction,receivedInstruction)==0)
					{
						instructionList[i].function();
						break;
					}
				}
				receivedValue = NOTSET;
				startInstructionReceive = 1;
				csr = 0;
				//Befehle Verarbeiten ENDE
			}
			
        }
	}
}