/*
####################################################################################################
Projektname:			UART Testsoftware

Autor:					Philip Urban 

Bearbeitungszeitraum:	ab 17.04.2011 

Projekt-ID:				

Mikrocontroller:		atmega32
SYSTEMTAKT:				8000000Hz

####################################################################################################
*/


#include <util/delay.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "lcd.h"
#include "uart.h"

#define UART_BAUD_RATE  9600

typedef unsigned int 	uint;
typedef unsigned char 	uchar;


void long_delay(uint16_t ms) {
 
    for(; ms>0; ms--) _delay_ms(1);
}

void lcd_vint(uint var){
    char Buffer[7];
	itoa(var, Buffer, 10);
	lcd_string( Buffer );
}

int main(void)
{	
	lcd_init();
	lcd_clear();
	//lcd_command(0x0F); //blinkender Cursor
	lcd_string("ATmega32 startup");
	
	DDRD &= ~(1<<DDD0);
	PORTD |= 1<<PD0;
	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
	sei();
	
	uchar c;
	
	
for(;;){
	
	//UART Datenverarbeitung
    c = uart_getc();

    if ( c & UART_NO_DATA )
        {
		/*no data*/
        }
        else if(c!=0)
        {
			uart_putc(c);
		}	
}
}